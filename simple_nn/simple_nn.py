import numpy as np
import matplotlib.pyplot as plt

# -- Activation functions --
def _sigmoid(x, deriv=False):
    res = 1./(1. + np.exp(-x))
    if deriv:
        res = np.multiply(res, (1. - res))
    return res

def _tanh(x, deriv=False):
    res = np.tanh(x)
    if deriv:
        res = 1. - np.power(res, 2.)
    return res

def _relu(x, deriv=False):
    if not deriv:
        res = np.multiply(x, (x > 0))
    else:
        res = np.multiply(1., (x >= 0))
    return res
# -- Activation functions --

def fprop(A_prev, W_l, b_l, nonlin):
    Z_l = np.dot(W_l, A_prev) + b_l
    A_l = nonlin(Z_l)
    
    return A_l, Z_l

def bprop(dA_l, Z_l, W_l, b_l, A_prev, m, nonlin):
    dZ_l = np.multiply(dA_l, nonlin(Z_l, True))
    dW_l = (1/m) * np.dot(dZ_l, A_prev.transpose())
    db_l = (1/m) * np.sum(dZ_l, axis = 1)
    
    dA_lmo = np.dot(W_l.transpose(), dZ_l)
    
    return dA_lmo, dW_l, db_l

def initialize_params(units_per_layer, n_x):
    init_layer_params = []
    for idx, layer in enumerate(units_per_layer):
        if idx != 0:
            n_prev = units_per_layer[idx-1]
        else:
            # Features from training set X
            n_prev = n_x

        n_l = units_per_layer[idx]

        W_l = np.random.randn(n_l, n_prev) * np.sqrt(2/n_prev)
        b_l = np.random.randn(n_l, 1) * np.sqrt(2/n_prev)

        init_layer_params.append({
            'weights': W_l,
            'bias': b_l
        })

    return init_layer_params

def compute_cost(A_L, Y, m):
    # Cross entropy loss
    J = (-1/m) * np.sum(np.multiply(Y, np.log(A_L)) + np.multiply((1-Y), np.log(1-A_L)))
    return J

class NeuralNetwork:
    def __init__(self, layers, units_per_layer, l_rate, n_iter):
        assert len(units_per_layer) == layers, "The layer numbers in input do not match"

        # Hyperparameters:
        self.L = layers
        self.units_per_layer = units_per_layer
        self.l_rate = l_rate
        self.n_iter = n_iter

        # Parameters       
        self.params = None
        self.cost = None
        self.train_cost = []

    def initialize_params(self, X):
        n_x = X.shape[0]
        self.params = initialize_params(self.units_per_layer, n_x)

    def update_params(self, dW, db):
        for idx,layer in enumerate(self.params):
            self.params[idx]['weights'] = self.params[idx]['weights'] - self.l_rate * dW[idx]
            self.params[idx]['bias'] = self.params[idx]['bias'] - self.l_rate * db[idx]

    def compute_cost(self, A_L, Y, m):
        self.cost = compute_cost(A_L, Y, m)

    def optimize(self, X, Y):
        m = X.shape[1]

        for i in range(1, self.n_iter):
            A = []
            Z = []
            A.append(X)
            Z.append(None)
            
            # Forward Propagation
            for idx, layer in enumerate(self.params):
                nonlin = _sigmoid if idx == self.L-1 else _relu
                A_l, Z_l = fprop(A[idx], layer['weights'], layer['bias'], nonlin)

                A.append(A_l)
                Z.append(Z_l)

            dA = []
            dW = []
            db = []
            # Last value of dA will be the slope of Loss -> L(y_h, y)
            dA.append((-Y/A[-1]) + ((1.-Y)/(1.-A[-1])))

            # Back Propagation
            rev_network = enumerate(reversed(self.params))
            for idx, layer in rev_network:
                nonlin = _sigmoid if idx == 0 else _relu

                A_prev = A[-2 - idx]
                dA_prev, dW_l, db_l = bprop(dA[-1 - idx], 
                                           Z[-1 - idx], 
                                           layer['weights'],
                                           layer['bias'],
                                           A_prev,
                                           m, 
                                           nonlin)

                dA.insert(0, dA_prev)
                dW.insert(0, dW_l)
                db.insert(0, db_l)
                
            # Gradient Descent
            self.update_params(dW, db)

            self.compute_cost(A[-1], Y, m)
            self.train_cost.append(self.cost)

    def predict(self, X):
        A = [X]
        for idx, layer in enumerate(self.params):
            nonlin = _sigmoid if idx == self.L-1 else _relu
            A_l, Z_l = fprop(A[idx], layer['weights'], layer['bias'], nonlin)
            A.append(A_l)

        A_L = A_l
        predictions = (A_L > 0.5)
        return predictions

def plot_decision_boundary(model, X, y):
    # Set min and max values and give it some padding
    x_min, x_max = X[0, :].min() - 1, X[0, :].max() + 1
    y_min, y_max = X[1, :].min() - 1, X[1, :].max() + 1
    h = 0.01
    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    # Predict the function value for the whole grid
    Z = model(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm)
    plt.ylabel('x2')
    plt.xlabel('x1')
    plt.scatter([X[0, :]], [X[1, :]], marker='o',c=y, cmap=plt.cm.gist_yarg)
